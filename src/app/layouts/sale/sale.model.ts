import {Moment} from 'moment';

export interface ISale {
  id?: number;
  clientId?: number;
  productId?: number;
  staffId?: number;
  productName?: string;
  totalPrice?: number;
  quantity?: number;
  saleDate?: Moment;
}

export class Sale implements ISale {
  constructor(
    public id?: number,
    public clientId?: number,
    public productId?: number,
    public staffId?: number,
    public productName?: string,
    public totalPrice?: number,
    public quantity?: number,
    public saleDate?: Moment
  ) {}
}
