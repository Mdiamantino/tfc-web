import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {isNullOrUndefined} from 'util';
import {SaleService} from './sale.service';
import {ProductService} from './../product/product.service';
import {ClientService} from './../client/client.service';
import {UserService} from '@/shared/services';
import {ISale} from './sale.model';
import {IProduct} from './../product/product.model';
import {IClient} from './../client/client.model';
import {IUser} from '@/shared/models';

@Component({
  selector: 'app-sale-form',
  templateUrl: './sale-form.component.html',
  styleUrls: ['./sale.scss'],
})
export class SaleFormComponent implements OnInit, OnDestroy {
  public sales: ISale;
  public products: IProduct[];
  public clients: IClient[];
  public users: IUser[];
  private unsubscribe: Subject<void> = new Subject<void>();

  constructor(
    protected activatedRoute: ActivatedRoute,
    private saleService: SaleService,
    private productService: ProductService,
    private clientService: ClientService,
    private userService: UserService,
    private toastr: ToastrService
  ) {}

  public ngOnInit(): void {
    this.activatedRoute.data.subscribe(({sale}) => {
      this.sales = sale;
      this.loadProducts();
      this.loadClients();
      this.loadUsers();
    });
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public saveSale(): void {
    if (this.sales.id === undefined) {
      this.saleService.create(this.sales).subscribe(
        res => {
          this.sales = res;
          this.saveSuccess();
        },
        error => {
          this.throwError();
        }
      );
    } else {
      this.saleService.update(this.sales).subscribe(
        res => {
          this.sales = res;
          this.saveSuccess();
        },
        error => {
          this.throwError();
        }
      );
    }
  }

  private loadProducts(): void {
    this.productService.getAll().subscribe(res => {
      this.products = res;
    });
  }

  private loadClients(): void {
    this.clientService.getAll().subscribe(res => {
      this.clients = res;
    });
  }

  private loadUsers(): void {
    this.userService.getAll().subscribe(res => {
      this.users = res;
    });
  }

  private saveSuccess(): void {
    this.toastr.success('Information saved successfully', 'Success');
  }

  private throwError(): void {
    this.toastr.error('An error occurred on the system. Please contact the system administrator ', 'Error');
  }
}
