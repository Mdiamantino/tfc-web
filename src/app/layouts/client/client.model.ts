import {Moment} from 'moment';

export interface IClient {
  id?: number;
  firstName?: string;
  lastName?: string;
  LocalDate?: Moment;
  nif?: number;
  contacts?: IContacts[];
  addresses?: IAddresses[];
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public LocalDate?: Moment,
    public nif?: number,
    public contacts?: IContacts[],
    public addresses?: IAddresses[]
  ) {}
}

export const enum ContactType {
  EMAIL = 'EMAIL',
  PHONE = 'PHONE',
  MOBILE = 'MOBILE',
}

export interface IContacts {
  id?: number;
  contactType?: ContactType;
  contact?: string;
}

export class Contacts implements IContacts {
  constructor(public id?: number, public contactType?: ContactType, public contact?: string) {}
}

export interface IAddresses {
  id?: number;
  adress?: string;
  postalCode?: string;
}

export class Addresses implements IAddresses {
  constructor(public id?: number, public adress?: string, public postalCode?: string) {}
}
