﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

import {IUser} from './../models';

@Injectable({providedIn: 'root'})
export class UserService {
  private readonly resourceUrl = environment.url + '/api/user';

  constructor(private http: HttpClient) {}

  getAll(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${this.resourceUrl}/list`);
  }

  findById(id: number): Observable<IUser> {
    return this.http.get<IUser>(`${this.resourceUrl}/${id}`);
  }

  create(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${this.resourceUrl}/save`, user);
  }

  update(user: IUser): Observable<IUser> {
    return this.http.put<IUser>(`${this.resourceUrl}/update`, user);
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.resourceUrl}/delete/${id}`);
  }
}
